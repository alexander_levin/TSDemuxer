#include "include/FileTSDemuxer.h"

int main (int argc, char* argv [])
{
    if (argc > 1)
    {
        FileTSDemuxer demuxer (argv [1]);
        demuxer.start ();
    }
    else
    {
        cout << "Please enter the file name!" << endl;
    }
    
    return 0;
}
