#include "../include/TSDemuxer.h"

#include <iostream>

TSDemuxer::TSDemuxer ()
{
}

void TSDemuxer::start ()
{
    cout << "Start demuxing" << endl;
    while (true)
    {
        try
        {
            if (get () == SYNC_BYTE)
            {
                _index = 0;
                if (this->unpackHeader ())
                {
                    if ((_header.adaptation == Adaptation::ADAPTATION_ONLY) ||
                        (_header.adaptation == Adaptation::ADAPTATION_PAYLOAD) )
                    {
                        this->unpackAdaptation ();
                    }
                    if ((_header.adaptation == Adaptation::PAYLOAD_ONLY) ||
                        (_header.adaptation == Adaptation::ADAPTATION_PAYLOAD) )
                    {
                        unpackPayload ();
                    }
                }
                
            }
            
        }
        catch (exception ex)
        {
            cout << "Finishing demuxing process..." << endl;
            break;
        }

        
    }    
}

bool TSDemuxer::unpackHeader ()
{
    _header.unpack (*this);
    _index += _header.getTableSize ();
    if (_header.pid == 0x1fff) // omit packets with pid = 0x1fff
        return false;
    return true;
}

void TSDemuxer::unpackAdaptation ()
{
    _adaptation.unpack (*this);
    _index += _adaptation.getTableSize ();
}

void TSDemuxer::unpackPayload ()
{
    if (_header.pid == 0)
    {
        // We have received psi
        if (_header.pusi)
        {
            // Unit start
            this->unpackPSI ();
        }
    }
    else
    {
        if (_programs.find (_header.pid) != _programs.end ())
        {
            // We have this program
            
            if (_programs [_header.pid]->getState () == TSProgram::CONFIGURING)
            {
                this->unpackPSI ();
            }
            else
            {
                this->unpackPES ();
            }
            
            
        }
    }
    
}

void TSDemuxer::unpackPSI ()
{
    seek (1); // Omit the first byte

    _index += 2;
    
    uint8_t tableId = get ();
    
    switch (tableId)
    {
        case 0: // PAT
        {        
            uint16_t sectionLength = ((get () << 8) | get ()) & 0x0fff;
            
            seek (5); // We will not check tableId and etc for know
            
            int cnt = (sectionLength - 5) / 4 - 1; // Because at the end we have CRC
            unordered_map<int, shared_ptr<TSProgram>> programsTmp;
            for (int i = 0;i < cnt;i++)
            {
                uint16_t programId = (get () << 8) | get ();
                uint16_t pmtId = ((get () << 8) | get ()) & 0x1fff;
                
//                cout << "TSProgram: " << (int)programId << ";PMT : " << (int)pmtId << endl;
                shared_ptr<TSProgram> program;
                if (programsTmp.find (programId) == programsTmp.end ())
                {
                    program = make_shared<TSProgram> (programId);
                    programsTmp [programId] = program;
                }
                else
                {
                    program = programsTmp [programId];
                }
                _programs [pmtId] = program;        
                _programs [pmtId]->setState (TSProgram::CONFIGURING);
            }            
            
            _index += 7 + cnt * 4;
            
            break; 
        }
        case 2: // PMT
        {
            _programs [_header.pid]->setState (TSProgram::CONFIGURED);
            
            uint16_t sectionLength = ((get () << 8) | get ()) & 0x0fff;
            //cout << "Section length: " << (int)sectionLength << endl;            
            
            uint16_t programId = (get () << 8) | get ();
            
            //cout << "PAT program id: " << programId << endl;
            
            seek (5);
            
            uint16_t infoLength = ((get () << 8) | get ()) & 0x0fff;
            
            seek (infoLength);
            _index += infoLength;// Moving to Elementary streams
            
            int limit = sectionLength - 9 - infoLength - 4;
            int pos = 0;
            
            _index += 11 + infoLength;
            
            while (pos < limit)
            {
                uint8_t eStreamType = get ();
                //cout << "Stream type: " << int(eStreamType) << endl;
                uint16_t streamId = ((get () << 8) | get ()) & 0x1fff;

                int descriptorLength = ((get () << 8) | get ()) & 0x0fff;
                _index += 5;
                
                if (descriptorLength > 0)
                {
                    seek (descriptorLength);
                    _index += descriptorLength;
                }
                
                pos += 5 + descriptorLength;
                
                if (_programs.find (streamId) != _programs.end ())
                {
                    // We will not update data streams info
                    continue;
                }
                
                switch (eStreamType)
                {
                    case MediaStream::STREAM_ADTS_AAC:
                    {
                        auto stream = make_shared<AACStream> (streamId);
                        if (_programs.find (_header.pid) != _programs.end ())
                        {
                            _programs [streamId] = _programs [_header.pid];
                            _programs [_header.pid]->addStream (stream);
                            _programs [_header.pid]->recordStream (streamId);
                        }
                        break;
                    }
                    case MediaStream::STREAM_VIDEO_H264:
                    {
                        auto stream = make_shared<H264Stream> (streamId);
                        if (_programs.find (_header.pid) != _programs.end ())
                        {
                            _programs [streamId] = _programs [_header.pid];
                            _programs [_header.pid]->addStream (stream);
                            _programs [_header.pid]->recordStream (streamId);
                        }                        
                        break;
                    }
                    default:
                    {
                        auto stream = make_shared<MediaStream> (streamId, eStreamType);
                        if (_programs.find (_header.pid) != _programs.end ())
                        {
                            _programs [streamId] = _programs [_header.pid];
                            _programs [_header.pid]->addStream (stream);
                            _programs [_header.pid]->recordStream (streamId);
                        }
                        break;
                    }
                }
                
                
            }
            
            
            break; 
        }
    }
    
    
}

void TSDemuxer::unpackPES ()
{
    if (_header.pusi)
    {
        
        if (NORMAL_TS_SIZE - 1 - _index >= 9)
        {
            seek (4);
            
            uint16_t length = (get () << 8) | get ();

            _index += 6;
            uint8_t mediaFlag = get () >> 6;
            //cout << "Media flag: " << (int)mediaFlag << endl;
            uint8_t headerLength = get ();
            _index += 2;
            if (mediaFlag == 2) 
            {
                seek (6); // omiting dts
                _index += 6;
            }
            else if (mediaFlag == 3)
            {
                seek (12); // omiting dts and pts
                _index += 12;
            }

        }
    }
    
    if (_programs.find (_header.pid) != _programs.end ())
    {
        _programs [_header.pid]->unpack (_header, NORMAL_TS_SIZE - 1 - _index, *this);
    }
}


