#include "../include/H264Stream.h"

H264Stream::H264Stream (int id):
    MediaStream (id, MediaStream::STREAM_VIDEO_H264)
{

}

string H264Stream::generateStreamName ()
{
    string name = string ("stream_") + std::to_string (_id) + "_h264";
    return move (name);    
}
