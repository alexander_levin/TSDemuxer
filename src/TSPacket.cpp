#include "../include/TSPacket.h"

Adaptation::Adaptation ()
{
}


void Adaptation::unpack (TSInputStream& stream)
{
    length = (size_t)stream.get ();
    if (length > 0)
    {
        uint8_t data = stream.get ();
        
        discontinuity = data >> 7;
        randomAccessIndicator = (data >> 6) & 0x1;
        priorityIndicator = (data >> 5) & 0x1;
        pcrFlag = (data >> 4) & 0x1;
        opcrFlag = (data >> 3) & 0x1;
        splicingPointFlag = (data >> 2) & 0x1;
        privateDataFlag = (data >> 1) & 0x1;
        extensionFlag = data & 0x1;
        
        stream.seek (length - 1);
    }    
}

int Adaptation::getTableSize () const
{
    return 1 + length;
}

//-----------------------------

TSPacketHeader::TSPacketHeader ()
{
    _data.resize (3);
}

void TSPacketHeader::unpack (TSInputStream& stream)
{
    stream.fill (_data.begin (), _data.end ());
    
    pid = (_data [0] << 8 | _data [1]) & 0x1fff;
    
    tei = _data [0] >> 7;
    pusi = (_data [0] >> 6) & 0x1;
    tsc = _data [2] >> 6;
    adaptation = (_data [2] >> 4) & 0x3;
    continuety = _data [2] & 0xf;    
}

int TSPacketHeader::getTableSize () const
{
    return 3;
}


//----------------------------------------


MediaFrame::MediaFrame ()
{
    _data.resize (PAYLOAD_BUFFER_SIZE);
    clear ();
}

void MediaFrame::read (TSInputStream& stream, int size)
{
    stream.fill (_data.begin () + _size, _data.begin () + _size + size);
    _size += size;
}

void MediaFrame::clear ()
{
    _size = 0;
}

int MediaFrame::getSize () const
{
    return _size;
}

vector<uint8_t>::iterator MediaFrame::begin ()
{
    return _data.begin ();
}

vector<uint8_t>::iterator MediaFrame::end ()
{
    return _data.begin () + _size;
}

