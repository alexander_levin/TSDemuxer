#include "../include/AACStream.h"

AACStream::AACStream (int id):
    MediaStream (id, MediaStream::STREAM_ADTS_AAC)
{

}

string AACStream::generateStreamName ()
{
    string name = string ("stream_") + std::to_string (_id) + "_aac";
    return move (name);    
}
