#include "../include/TSProgram.h"

TSProgram::TSProgram (int id):
    _id (id),
    _state (CONFIGURING)
{

}

TSProgram::~TSProgram ()
{
}
        
int TSProgram::getId () const
{
    return _id;
}
 
int TSProgram::getState () const
{
    return _state;
}

void TSProgram::setState (int state)
{
    _state = state;
}
 
void TSProgram::addStream (shared_ptr<MediaStream> stream)
{
    _mediaStreams [stream->getId ()] = stream;
}

void TSProgram::recordStream (int streamId)
{
    auto stream = _mediaStreams.find (streamId);
    if (stream != _mediaStreams.end ())
    {
        stream->second->startRecording ();
    }
}

void TSProgram::unpack (TSPacketHeader& header, int size, TSInputStream& tstream)
{
    auto stream = _mediaStreams.find (header.pid);
    if (stream != _mediaStreams.end ())
    {
        stream->second->unpack (header, size, tstream);
    }        
}


