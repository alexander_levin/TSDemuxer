#include "../include/FileTSDemuxer.h"

#include <iostream>

FileTSDemuxer::FileTSDemuxer (const char* filename):
    _file (nullptr),
    _fileName (filename)
{
    _file = fopen (_fileName.c_str (), "r");
    if (_file == nullptr)
    {
        throw runtime_error ("Could not open file");
    }    
}

FileTSDemuxer::~FileTSDemuxer ()
{
    if (_file != nullptr)
    {
        fclose (_file);
    }
}

uint8_t FileTSDemuxer::get ()
{
    uint8_t data;
    size_t readCnt = fread (&data, sizeof (uint8_t), 1, _file);
    if (readCnt != 1)
    {
        throw runtime_error ("Cannot read file");
    }
    return move (data);
}

void FileTSDemuxer::fill (vector<uint8_t>::iterator begin, vector<uint8_t>::iterator end)
{
    size_t readCnt = 0;
    int size = end - begin;
    readCnt = fread (&(*begin), sizeof (uint8_t), size, _file);
    if (readCnt != size)
    {
        throw runtime_error ("Cannot read file");
    }    
}

void FileTSDemuxer::seek (int pos)
{
    fseek (_file, pos, SEEK_CUR);
}
