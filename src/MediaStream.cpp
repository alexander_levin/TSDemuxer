#include "../include/MediaStream.h"


MediaStream::MediaStream (int id, int type):
    _id (id),
    _type (type),
    _continuety (-1)
{
    _isRecorderStarted.store (false);
    _canConsume  = false;
    _errorDetected = false;
    _isUnitStart = false;
}

MediaStream::~MediaStream ()
{
    _isRecorderStarted.store (false);
    _condVar.notify_one ();
    _recorder.get ();
}

int MediaStream::getId () const
{
    return _id;
}

int MediaStream::getType () const
{
    return _type;
}

void MediaStream::startRecording ()
{
    if (!_isRecorderStarted.load ())
    {
        _isRecorderStarted.store (true);
        _recorderThread = make_shared<StreamRecorder> (*this, _mutex);
        _recorder = async (launch::async, ref (*_recorderThread));
    }
}

void MediaStream::stopRecording ()
{
    _isRecorderStarted.store (false);
    _recorder.get ();
    
}

void MediaStream::reset ()
{
    _continuety = -1;
}

void MediaStream::unpack (TSPacketHeader& header, int size, TSInputStream& tstream)
{
    std::unique_lock<mutex> locker (_mutex);
    _condVar.wait (locker, [&](){ return !_canConsume;  });
    
    checkContinuety (header.continuety);
    if (header.pusi)
    {
        if (!_isUnitStart)
        {
            _isUnitStart = true;             
            _frame.read (tstream, size); 
        }
        else
        {
            if (_errorDetected)
            {
                _errorDetected = false;
                _frame.clear ();
                _frame.read (tstream, size); 
            }
            else
            {
                _frame.read (tstream, size); 
                _canConsume = true;
                _isUnitStart = false;
                locker.unlock ();
                _condVar.notify_one ();              
            }
        }
    }
    else
    {
        _frame.read (tstream, size);    
    }    
}

bool MediaStream::checkContinuety (int continuety)
{
    if (_continuety == -1)
    {
        _continuety = continuety;
        return true;
    }
    else
    {
        int continuetyPrediction = (_continuety + 1) & 0x0f;
        _continuety = continuety;
        
        if (continuetyPrediction != continuety)
        {
            cout << "Warning: packet continuety does not match to predicted!" << endl;
            _errorDetected = true;
        }
        return (continuetyPrediction == continuety);
    }
}

string MediaStream::generateStreamName ()
{
    string name;
    
    name = string ("stream_") + std::to_string (_id) + "_" + std::to_string (_type);
    
    return move (name);
}



