# Sample TS demuxer

## Description

Sample ts file converter. Pull out streams from ts packet and write to separate files.

**Warning: this program does not inspect ts payload for wrong frames and errors!**


## Installation

cd build

make


## Usage

```
cd build

./TSDemuxer ../samples/test.ts

```


