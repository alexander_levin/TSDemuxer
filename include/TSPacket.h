#ifndef TS_PACKET_H
#define TS_PACKET_H

#define SYNC_BYTE 0x47
#define NORMAL_TS_SIZE 188
#define PAYLOAD_BUFFER_SIZE 2097152 // 2MB buffer size

#include <vector>
#include <iostream>

#include "TSInputStream.h"

using namespace std;

enum class Scrambling
{
    RESERVED = 0x40,
    EVEN = 0x80,
    ODD = 0xC0
};

class TSTable
{
    public:
        virtual ~TSTable (){}
        
        virtual void unpack (TSInputStream& stream) = 0;
        virtual int getTableSize () const = 0;
    protected:
        vector<uint8_t> _data;
};


class Adaptation:
    public TSTable
{
    public:
        enum
        {
            PAYLOAD_ONLY = 1,
            ADAPTATION_ONLY = 2,
            ADAPTATION_PAYLOAD = 3        
        };    

        virtual void unpack (TSInputStream& stream) override;
        virtual int getTableSize () const override;
        
    public:
        Adaptation ();    
        uint8_t length;
        bool discontinuity;
        bool randomAccessIndicator;
        bool priorityIndicator;
        bool pcrFlag;
        bool opcrFlag;
        bool splicingPointFlag;
        bool privateDataFlag;
        bool extensionFlag;
};


class TSPacketHeader:
    public TSTable
{
    public:
        TSPacketHeader ();
        
        virtual void unpack (TSInputStream& stream) override;
        virtual int getTableSize () const override;
        
        bool tei;
        bool pusi;
        bool priority;
        uint16_t pid;
        uint8_t tsc;
        uint8_t adaptation;
        int continuety;
    
};

class MediaFrame
{
    public:
        MediaFrame ();

        void clear ();
        void read (TSInputStream& stream, int size);
        
        int getSize () const;
        
        vector<uint8_t>::iterator begin ();       
        vector<uint8_t>::iterator end ();    
    private:
        int _size;
        vector<uint8_t> _data;
};

struct ProgramAssociationTable
{
    
};

struct ConditionalAccessTable
{
    
};

struct ProgramMapTable
{
    
};

struct NetworkInformationTable
{
    
};



#endif
