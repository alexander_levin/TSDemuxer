#ifndef TS_PROGRAM_H
#define TS_PROGRAM_H

#include <vector>
#include <memory>
#include <algorithm>
#include <unordered_map>

#include "MediaStream.h"
#include "TSInputStream.h"
#include "TSPacket.h"

using namespace std;

class TSProgram
{
    public:
        enum State
        {
            CONFIGURING,
            CONFIGURED
        };
        TSProgram (int id);
        ~TSProgram ();
        
        int getId () const;
        
        int getState () const;
        void setState (int state);
        
        void addStream (shared_ptr<MediaStream> stream);
        void recordStream (int streamId);
        
        void unpack (TSPacketHeader& header, int size, TSInputStream& tstream);
    private:
        int _id;
        int _state;
        unordered_map<int, shared_ptr<MediaStream>> _mediaStreams;
};


#endif
