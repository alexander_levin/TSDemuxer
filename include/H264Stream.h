#ifndef H264_STREAM_H
#define H264_STREAM_H


#include "MediaStream.h"


class H264Stream:
    public MediaStream
{
    public:
        H264Stream (int id);
        virtual string generateStreamName () override;
};



#endif
