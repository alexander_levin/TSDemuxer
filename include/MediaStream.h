#ifndef MEDIA_STREAM_H
#define MEDIA_STREAM_H

#include <queue>
#include <memory>
#include <atomic>
#include <condition_variable>
#include <stdio.h>
#include <future>
#include <stdio.h>
#include <string.h>
#include <functional>
#include <iostream>

#include "TSInputStream.h"
#include "TSPacket.h"

using namespace std;

class MediaStream
{
    class StreamRecorder
    {
        public:
            StreamRecorder (MediaStream& stream, mutex& mu):
                _stream (stream),
                _mutex (mu)
            {
                std::cout << "StreamRecorder ()" << std::endl;
            }
            
            ~StreamRecorder ()
            {
                std::cout << "~StreamRecorder ()" << std::endl;
            }
                        
            void operator() ()
            {
                FILE* fl = fopen (_stream.generateStreamName ().c_str (), "w+");
                if (fl != nullptr)
                {
                    while (_stream._isRecorderStarted.load ())
                    {
                        unique_lock<mutex> locker (_mutex);
                        _stream._condVar.wait (locker, [&](){
                            return _stream._canConsume || !_stream._isRecorderStarted.load ();
                        });
                        auto startPos = _stream._frame.begin ();
                        int size = _stream._frame.getSize ();
                        int cnt = fwrite (&(*startPos), sizeof (uint8_t), size, fl);
                        if (cnt != size)
                        {
                            cout << "Stream store error!" << endl;
                            break;
                        }    
                        _stream._frame.clear ();
                        _stream._canConsume = false;
                        locker.unlock ();
                        _stream._condVar.notify_one ();
                    }
                    fclose (fl);
                }    
            }
        private:
            MediaStream& _stream;
            mutex& _mutex;
    };

    public:
        enum Type
        {
            STREAM_VIDEO_MPEG_1 = 0x01,
            STREAM_VIDEO_MPEG_2 = 0x02,
            STREAM_AUDIO_MPEG_1 = 0x03,
            STREAM_AUDIO_MPEG_2 = 0x04,
            STREAM_TABLED_MPEG_2 = 0x05,
            STREAM_PACKETIZED_MPEG_2 = 0x06,
            STREAM_MHEG = 0x07,
            STREAM_DSM = 0x08,
            STREAM_DSM_MULTIPROTOCOL_ENCAPSULATION = 0x0A,
            STREAM_DSM_UN_MESSAGES = 0x0B,
            STREAM_DSM_DESCRIPTORS = 0x0C,
            STREAM_DSM_TABLED_DATA = 0x0D,
            STREAM_ADTS_AAC = 0x0F,
            STREAM_VIDEO_MPEG_4 = 0x10,
            STREAM_AUDIO_MPEG_4_LOAS = 0x11,
            STREAM_VIDEO_H264 = 0x1B,
            STREAM_VIDEO_H265 = 0x24,
        };
        
        MediaStream (int id, int type);
        virtual ~MediaStream ();

        int getId () const;
        int getType () const;
        
        void startRecording ();
        void stopRecording ();
        
        void unpack (TSPacketHeader& header, int size, TSInputStream& tstream);
        void reset ();
        
        virtual string generateStreamName ();
    protected:
        bool checkContinuety (int continuety);
    protected:
        int _id;
        int _type;
        MediaFrame _frame;
        atomic<bool> _isRecorderStarted;
        condition_variable _condVar; 
        mutex _mutex;
        future<void> _recorder;
        bool _canConsume;
        int _continuety;
        bool _errorDetected;
        bool _isUnitStart;
        shared_ptr<StreamRecorder> _recorderThread;
};



#endif
