#ifndef AAC_STREAM_H
#define AAC_STREAM_H


#include "MediaStream.h"


class AACStream:
    public MediaStream
{
    public:
        AACStream (int id);
        
        virtual string generateStreamName () override;
};



#endif
