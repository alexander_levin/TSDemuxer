#ifndef TS_INPUT_STREAM_H
#define TS_INPUT_STREAM_H

#include <stdint.h>
#include <vector>

using namespace std;

class TSInputStream
{
    public:
        virtual ~TSInputStream (){}

        virtual uint8_t get () = 0;
        virtual void seek (int pos) = 0;
        virtual void fill (vector<uint8_t>::iterator begin, vector<uint8_t>::iterator end) = 0;        
};


#endif
