#ifndef FILE_TS_DEMUXER_H
#define FILE_TS_DEMUXER_H

#include <stdio.h>
#include <string>
#include <exception>
#include <bitset>
#include <memory>

#include "TSDemuxer.h"

using namespace std;


class FileTSDemuxer:
    public TSDemuxer
{
    public:
        FileTSDemuxer (const char* filename);
        ~FileTSDemuxer ();
    protected:
        virtual uint8_t get () override;
        virtual void fill (vector<uint8_t>::iterator begin, vector<uint8_t>::iterator end) override;
        virtual void seek (int pos) override;
    private:
        FILE* _file;
        string _fileName;
};

#endif
