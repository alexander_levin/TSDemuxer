#ifndef TS_DEMUXER_H
#define TS_DEMUXER_H

#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>

#include "TSPacket.h"
#include "MediaStream.h"
#include "AACStream.h"
#include "H264Stream.h"
#include "TSProgram.h"
#include "TSInputStream.h"


using namespace std;

class TSDemuxer:
    public TSInputStream
{
    public:
        TSDemuxer ();
        
        void start ();
        
    protected:
        bool unpackHeader ();
        void unpackAdaptation ();
        void unpackPayload ();
        void unpackPSI ();
        void unpackPES ();
    protected:
        unordered_map<int,shared_ptr<TSProgram>> _programs;
        TSPacketHeader _header;
        Adaptation _adaptation;
        int _index;
        unordered_map<int,bool> _pes;
};

#endif
